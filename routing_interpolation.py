import numpy as np
import matplotlib.pyplot as plt
from scipy.interpolate import CubicSpline
import sys

# read in file from command line
with open(sys.argv[1]) as f:
    content = f.readlines()

content = [x.strip() for x in content] #removing stuff

# pull segments
keep_data = []
temp_list = []
for i in content:
    if '.' in i and 'l' not in i and 't' not in i:
        temp_list.append(i)
    else:
        keep_data.append(temp_list)
        temp_list = []
keep_data = [k for k in keep_data if len(k) > 0]


# convect segments to arrays
data_to_num = []
for seg in keep_data:
    data = []
    for p in seg:
        data.append(eval(p))
    data_to_num.append(data)

    
list_of_segment_arrays = []
for i in data_to_num:
    segment_points = []
    for j in i:
        for k in j:
            segment_points.append(k)
    list_of_segment_arrays.append(segment_points)
list_of_segment_arrays = [np.array(k) for k in list_of_segment_arrays] #finally a list of arrays

#sort the segment for the spline
sorted_segment = np.sort(list_of_segment_arrays[0],axis=1)[::-1]

#build spline
spline = CubicSpline(x=sorted_segment[:,0],y=sorted_segment[:,1])

#get min and max x values and use Y to interpolate
min_val = np.min(sorted_segment[:,0])
max_val = np.max(sorted_segment[:,0])
new_x = np.linspace(start=min_val,stop=max_val,num=int(sys.argv[2]))
new_y = spline(new_x)


#output to text file
f = open('output.txt', 'w')
for t in list(zip(new_x,new_y)):
    f.write(' '.join(str(s) for s in t) + '\n')