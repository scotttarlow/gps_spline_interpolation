
 ```python routing_interpolation.py Untitled.txt 100```
   where `Untiled.txt` is a text file with the original vertices and `100` are the number of points (must be an integer) you want between your start and end point. The program outputs a `output.txt` file which contains the new vertices.
